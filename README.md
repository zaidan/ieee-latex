# ltx - IEEE LaTeX Skeleton 
This is an IEEE LaTeX skeleton using the [IEEEtran
template](https://www.ctan.org/tex-archive/macros/latex/contrib/IEEEtran) with
[BibLATEX](https://www.ctan.org/pkg/biblatex) and
[ltx](https://github.com/zaidan/ltx).

## Credits

 * [Firas Zaidan](https://github.com/zaidan)

## License

See `LICENSE` file.
